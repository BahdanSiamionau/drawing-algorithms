#include "mainwindow.h"
#include <QMouseEvent>
#include <QDebug>
#include <QPicture>
#include <QLayout>
#include <QMenu>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    area = new PaintArea();
    setCentralWidget(area);
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(showContextMenu(const QPoint&)));
}

MainWindow::~MainWindow()
{
    
}

void MainWindow::showContextMenu(const QPoint &point)
{
    QPoint globalPos = this->mapToGlobal(point);
    QAction *action;

    QMenu *menu = new QMenu();
    action = menu->addAction("Zoom in");
    connect(action, SIGNAL(triggered()), this, SLOT(zoomIn()));
    action = menu->addAction("Zoom out");
    connect(action, SIGNAL(triggered()), this, SLOT(zoomOut()));
    menu->addSeparator();
    QMenu *submenu = new QMenu("Alghoritm");
    action = submenu->addAction("Brasenham");
    connect(action, SIGNAL(triggered()), this, SLOT(brasenham()));
    action = submenu->addAction("DDA");
    connect(action, SIGNAL(triggered()), this, SLOT(DDA()));
    action = submenu->addAction("Wu");
    connect(action, SIGNAL(triggered()), this, SLOT(wu()));
    menu->addMenu(submenu);
    menu->addSeparator();
    action = menu->addAction("About");
    connect(action, SIGNAL(triggered()), this, SLOT(about()));
    action = menu->addAction("Exit");
    connect(action, SIGNAL(triggered()), this, SLOT(exit()));
    menu->exec(globalPos);
}

void MainWindow::zoomIn()
{
    area->zoomIn();
}

void MainWindow::zoomOut()
{
    area->zoomOut();
}

void MainWindow::brasenham()
{
    area->setAlgorithm("brasenham");
}

void MainWindow::DDA()
{
    area->setAlgorithm("dda");
}

void MainWindow::wu()
{
    area->setAlgorithm("wu");
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About"),
                       QString("Program presented by Bahdan Siamionau in context of "
                               "laboratory practice during autumn term 2012/2013"));
}

void MainWindow::exit()
{
    this->close();
}
