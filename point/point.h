#ifndef POINT_H
#define POINT_H

#include <QPoint>

class Point
{

public:
    Point();
    QPoint getPoint() const;
    void setPoint(QPoint);
    double getAlpha() const;
    void setAlpha(double);

private:
    QPoint point;
    double alpha;
};

#endif // POINT_H
