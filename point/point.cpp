#include "point.h"

Point::Point()
{
}

QPoint Point::getPoint() const
{
    return point;
}

void Point::setPoint(QPoint p)
{
    point = p;
}

double Point::getAlpha() const
{
    return alpha;
}

void Point::setAlpha(double a)
{
    alpha = a;
}
