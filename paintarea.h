#ifndef PAINTAREA_H
#define PAINTAREA_H

#include <algorithms/abstractalgorithm.h>
#include <algorithms/brasenham.h>
#include <algorithms/dda.h>
#include <algorithms/wu.h>
#include <QWidget>
#include <QList>
#include <point/point.h>

class PaintArea : public QWidget
{
    Q_OBJECT

public:
    explicit PaintArea(QWidget *parent = 0);
    void setAlgorithm(QString);
    void zoomIn();
    void zoomOut();

protected:
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);

private:
    void drawGrid(QPainter&);
    bool leftButtonPressed;
    int size;
    QPainter *painter;
    QPoint *fromPoint;
    QPoint *toPoint;
    QList<Point> points;
    AbstractAlgorithm *algorithm;
};

#endif // PAINTAREA_H
