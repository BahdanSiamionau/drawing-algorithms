#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "paintarea.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    PaintArea *area;

private slots:
    void showContextMenu(const QPoint &);
    void zoomIn();
    void zoomOut();
    void brasenham();
    void DDA();
    void wu();
    void exit();
    void about();
};

#endif // MAINWINDOW_H
