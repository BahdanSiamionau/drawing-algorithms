#ifndef WU_H
#define WU_H

#include "abstractalgorithm.h"

class Wu : public AbstractAlgorithm
{
public:
    Wu();
    QList<Point> drawLine(QPoint&, QPoint&, int);

private:
    double floatPart(double, int);
};

#endif // WU_H
