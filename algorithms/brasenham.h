#ifndef BRASENHAM_H
#define BRASENHAM_H

#include "abstractalgorithm.h"

class Brasenham : public AbstractAlgorithm
{
public:
    Brasenham();
    QList<Point> drawLine(QPoint&, QPoint&, int);

private:
    double round(double, int);
};

#endif // BRASENHAM_H
