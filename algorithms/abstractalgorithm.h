#ifndef ABSTRACTALGORITHM_H
#define ABSTRACTALGORITHM_H

#include <QObject>
#include <QList>
#include <QPoint>
#include "point/point.h"

class AbstractAlgorithm : public QObject
{
    Q_OBJECT

public:
    virtual QList<Point> drawLine(QPoint&, QPoint&, int) = 0;
};

#endif // ABSTRACTALGORITHM_H
