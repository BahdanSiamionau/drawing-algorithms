#ifndef DDA_H
#define DDA_H

#include "abstractalgorithm.h"

class DDA : public AbstractAlgorithm
{
public:
    DDA();
    QList<Point> drawLine(QPoint&, QPoint&, int);
};

#endif // DDA_H
