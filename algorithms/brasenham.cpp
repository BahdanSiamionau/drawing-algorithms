#include "brasenham.h"
#include <QDebug>
#include <cmath>

Brasenham::Brasenham()
{

}

QList<Point> Brasenham::drawLine(QPoint &point1, QPoint &point2, int size)
{
    point1.setX(point1.x() - point1.x() % size);
    point1.setY(point1.y() - point1.y() % size);
    point2.setX(point2.x() - point2.x() % size);
    point2.setY(point2.y() - point2.y() % size);

    int deltaX = abs(point2.x() - point1.x());
    int deltaY = abs(point2.y() - point1.y());

    int dX = (point2.x() >= point1.x() ? size : -size);
    int dY = (point2.y() >= point1.y() ? size : -size);

    int deltaMax = (deltaX >= deltaY ? deltaX : deltaY);

    QList<Point> list;
    QPoint qpoint;
    Point point;
    if (deltaY <= deltaX)
    {
        qpoint.setX(point1.x());
        qpoint.setY(point1.y());
        int error = -deltaX;

        while (deltaMax >= 0)
        {
            point.setPoint(qpoint);
            point.setAlpha(1);
            list.append(point);
            qpoint.setX(qpoint.x() + dX);
            error += 2 * deltaY;
            if (error > 0)
            {
                error -= 2 * deltaX;
                qpoint.setY(qpoint.y() + dY);
            }
            deltaMax -= size;
        }
    }
    else
    {
        qpoint.setX(point1.x());
        qpoint.setY(point1.y());
        int error = -deltaY;

        while (deltaMax >= 0)
        {
            point.setPoint(qpoint);
            point.setAlpha(1);
            list.append(point);
            qpoint.setY(qpoint.y() + dY);
            error += 2 * deltaX;
            if (error > 0)
            {
                error -= 2 * deltaY;
                qpoint.setX(qpoint.x() + dX);
            }
            deltaMax -= size;
        }
    }
    return list;
}
