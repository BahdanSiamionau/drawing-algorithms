#include "dda.h"
#include "cmath"
#include <QDebug>

DDA::DDA()
{

}

QList<Point> DDA::drawLine(QPoint &point1, QPoint &point2, int size)
{
    int x1 = point1.x() - point1.x() % size;
    int y1 = point1.y() - point1.y() % size;
    int x2 = point2.x() - point2.x() % size;
    int y2 = point2.y() - point2.y() % size;

    int deltaMax = (abs(x2 - x1) >= abs(y2 - y1) ? abs(x2 - x1) : abs(y2 - y1));
    if (deltaMax == 0)
    {
        deltaMax = 1;
    }

    double dX = (x2 - x1) / ((double)deltaMax/size);
    double dY = (y2 - y1) / ((double)deltaMax/size);

    double x = point1.x();
    double y = point1.y();

    QList<Point> list;
    QPoint qpoint;
    Point point;
    while(deltaMax > 0)
    {
        qpoint.setX(x - (int)x % size);
        qpoint.setY(y - (int)y % size);

        point.setPoint(qpoint);
        point.setAlpha(1);
        list.append(point);

        x += dX;
        y += dY;

        deltaMax -= size;
    }
    qpoint.setX(x - (int)x % size);
    qpoint.setY(y - (int)y % size);

    point.setPoint(qpoint);
    point.setAlpha(1);
    list.append(point);
    return list;
}
