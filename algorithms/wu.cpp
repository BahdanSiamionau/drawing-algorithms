#include "wu.h"
#include "cmath"
#include <QDebug>

Wu::Wu()
{

}

QList<Point> Wu::drawLine(QPoint &point1, QPoint &point2, int size)
{
    double dX = abs(point2.x() - point1.x());
    double dY = abs(point2.y() - point1.y());

    QList<Point> list;
    QPoint qpoint;
    Point point;
    if(dX > dY)
    {
        if(point2.x() < point1.x())
        {
            dX = point1.x() - point2.x();
            dY = point1.y() - point2.y();
            double gradient = dY / dX;

            int xend, yend, xpxl1, ypxl1;
            xend = xpxl1 = point2.x();
            yend = ypxl1 = point2.y();
            double xgap = size - floatPart(point2.x() + size/2, size);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY(ypxl1 - ypxl1 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point1.y(), size) * xgap / (size*size));
            list.append(point);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY((ypxl1 - ypxl1 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point1.y(), size) * xgap / (size*size));
            list.append(point);

            double intery = yend + gradient*size;

            int xpxl2, ypxl2;
            xend = xpxl2 = point1.x();
            yend = ypxl2 = point1.y();
            xgap = floatPart(point1.x() + size/2, size);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY(ypxl2 - ypxl2 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point1.y(), size) * xgap / (size*size));
            list.append(point);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY((ypxl2 - ypxl2 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point1.y(), size) * xgap / (size*size));
            list.append(point);

            for (int i = (xpxl1 - xpxl1 % size) + size; i <= xpxl2 - size; i+=size)
            {
                qpoint.setX(i);
                qpoint.setY(intery - (int)intery % size);
                point.setPoint(qpoint);
                point.setAlpha(1 - floatPart(intery, size)/size);
                list.append(point);

                qpoint.setX(i);
                qpoint.setY(intery - (int)intery % size + size);
                point.setPoint(qpoint);
                point.setAlpha(floatPart(intery, size)/size);
                list.append(point);

                intery += gradient*size;
            }
        }
        else
        {
            dX = point2.x() - point1.x();
            dY = point2.y() - point1.y();
            double gradient = dY / dX;

            int xend, yend, xpxl1, ypxl1;
            xend = xpxl1 = point1.x();
            yend = ypxl1 = point1.y();
            double xgap = size - floatPart(point1.x() + size/2, size);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY(ypxl1 - ypxl1 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point2.y(), size) * xgap / (size*size));
            list.append(point);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY((ypxl1 - ypxl1 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point2.y(), size) * xgap / (size*size));
            list.append(point);

            double intery = yend + gradient*size;

            int xpxl2, ypxl2;
            xend = xpxl2 = point2.x();
            yend = ypxl2 = point2.y();
            xgap = floatPart(point2.x() + size/2, size);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY(ypxl2 - ypxl2 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point2.y(), size) * xgap / (size*size));
            list.append(point);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY((ypxl2 - ypxl2 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point2.y(), size) * xgap / (size*size));
            list.append(point);

            for (int i = (xpxl1 - xpxl1 % size) + size; i <= xpxl2 - size; i+=size)
            {
                qpoint.setX(i);
                qpoint.setY(intery - (int)intery % size);
                point.setPoint(qpoint);
                point.setAlpha(1 - floatPart(intery, size)/size);
                list.append(point);

                qpoint.setX(i);
                qpoint.setY(intery - (int)intery % size + size);
                point.setPoint(qpoint);
                point.setAlpha(floatPart(intery, size)/size);
                list.append(point);

                intery += gradient*size;
            }
        }
    }
    else
    {
        if(point2.y() < point1.y())
        {
            dX = point1.x() - point2.x();
            dY = point1.y() - point2.y();
            double gradient = dX / dY;

            int xend, yend, xpxl1, ypxl1;
            xend = xpxl1 = point2.x();
            yend = ypxl1 = point2.y();
            double ygap = size - floatPart(point2.y() + size/2, size);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY(ypxl1 - ypxl1 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point1.y(), size) * ygap / (size*size));
            list.append(point);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY((ypxl1 - ypxl1 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point1.y(), size) * ygap / (size*size));
            list.append(point);

            double intery = xend + gradient*size;

            int xpxl2, ypxl2;
            xend = xpxl2 = point1.x();
            yend = ypxl2 = point1.y();
            ygap = floatPart(point1.y() + size/2, size);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY(ypxl2 - ypxl2 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point1.y(), size) * ygap / (size*size));
            list.append(point);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY((ypxl2 - ypxl2 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point1.y(), size) * ygap / (size*size));
            list.append(point);

            for (int i = (ypxl1 - ypxl1 % size) + size; i <= ypxl2 - size; i+=size)
            {
                qpoint.setX(intery - (int)intery % size);
                qpoint.setY(i);
                point.setPoint(qpoint);
                point.setAlpha(1 - floatPart(intery, size)/size);
                list.append(point);

                qpoint.setX(intery - (int)intery % size + size);
                qpoint.setY(i);
                point.setPoint(qpoint);
                point.setAlpha(floatPart(intery, size)/size);
                list.append(point);

                intery += gradient*size;
            }
        }
        else
        {
            dX = point2.x() - point1.x();
            dY = point2.y() - point1.y();
            double gradient = dX / dY;

            int xend, yend, xpxl1, ypxl1;
            xend = xpxl1 = point1.x();
            yend = ypxl1 = point1.y();
            double ygap = size - floatPart(point1.y() + size/2, size);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY(ypxl1 - ypxl1 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point2.y(), size) * ygap / (size*size));
            list.append(point);

            qpoint.setX(xpxl1 - xpxl1 % size);
            qpoint.setY((ypxl1 - ypxl1 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point2.y(), size) * ygap / (size*size));
            list.append(point);

            double intery = xend + gradient*size;

            int xpxl2, ypxl2;
            xend = xpxl2 = point2.x();
            yend = ypxl2 = point2.y();
            ygap = floatPart(point2.y() + size/2, size);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY(ypxl2 - ypxl2 % size);
            point.setPoint(qpoint);
            point.setAlpha(1 - floatPart(point2.y(), size) * ygap / (size*size));
            list.append(point);

            qpoint.setX(xpxl2 - xpxl2 % size);
            qpoint.setY((ypxl2 - ypxl2 % size) + size);
            point.setPoint(qpoint);
            point.setAlpha(floatPart(point2.y(), size) * ygap / (size*size));
            list.append(point);

            for (int i = (ypxl1 - ypxl1 % size) + size; i <= ypxl2 - size; i+=size)
            {
                qpoint.setX(intery - (int)intery % size);
                qpoint.setY(i);
                point.setPoint(qpoint);
                point.setAlpha(1 - floatPart(intery, size)/size);
                list.append(point);

                qpoint.setX(intery - (int)intery % size + size);
                qpoint.setY(i);
                point.setPoint(qpoint);
                point.setAlpha(floatPart(intery, size)/size);
                list.append(point);

                intery += gradient*size;
            }
        }
    }
    return list;
}

double Wu::floatPart(double value, int size)
{
    return (int)value % size;
}
