#include "paintarea.h"
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>
#include <QPaintEvent>

PaintArea::PaintArea(QWidget *parent) :
    QWidget(parent)
{
    size = 8;
    fromPoint = new QPoint();
    toPoint = new QPoint();
    leftButtonPressed = false;
    setAlgorithm("brasenham");
}

void PaintArea::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        *fromPoint = event->pos();
        leftButtonPressed = true;
    }
}

void PaintArea::paintEvent(QPaintEvent *)
{
    painter = new QPainter(this);
    if (*fromPoint != *toPoint)
    {
        points = algorithm->drawLine(*fromPoint, *toPoint, size);
        int length = points.length();
        if (size > 1)
        {
            for(int i = 0; i < length; i++)
            {
                QPoint point;
                point.setX(points.at(i).getPoint().x() + size - 1);
                point.setY(points.at(i).getPoint().y() + size - 1);
                painter->setBrush(QColor(0, 0, 0, 255*points.at(i).getAlpha()));
                painter->drawRect(QRect(points.at(i).getPoint(), point));
            }
        }
        else
        {
            for(int i = 0; i < length; i++)
            {
                painter->setBrush(QColor(0, 0, 0, 255*points.at(i).getAlpha()));
                painter->drawPoint(points.at(i).getPoint());
            }
        }
    }
    this->drawGrid(*painter);
    painter->end();
}

void PaintArea::mouseMoveEvent(QMouseEvent *event)
{
    if (leftButtonPressed)
    {
        *toPoint = event->pos();
        this->repaint();
    }
}

void PaintArea::mouseReleaseEvent(QMouseEvent *)
{
    toPoint = new QPoint();
    fromPoint = new QPoint();
    leftButtonPressed = false;
    this->repaint();
}

void PaintArea::drawGrid(QPainter &painter)
{
    if (size > 1)
    {
        int width = this->width();
        int height = this->height();
        for (int x = 0; x < width; x+=size)
        {
            painter.drawLine(x, 0, x, height);
        }
        for (int y = 0; y < height; y+=size)
        {
            painter.drawLine(0, y, width, y);
        }
    }
}

void PaintArea::setAlgorithm(QString name)
{
    if (name == "brasenham")
    {
        algorithm = new Brasenham();
    }
    else if (name == "dda")
    {
        algorithm = new DDA();
    }
    else
    {
        algorithm = new Wu();
    }
}

void PaintArea::zoomIn()
{
    if (size != 32)
    {
        size *= 2;
    }
    this->repaint();
}

void PaintArea::zoomOut()
{
    if (size != 1)
    {
        size /= 2;
    }
    this->repaint();
}
