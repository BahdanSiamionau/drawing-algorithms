#-------------------------------------------------
#
# Project created by QtCreator 2012-10-14T14:55:09
#
#-------------------------------------------------

QT       += core gui

TARGET = PaintingAlgorithms
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintarea.cpp \
    algorithms/brasenham.cpp \
    algorithms/dda.cpp \
    algorithms/wu.cpp \
    point/point.cpp

HEADERS  += mainwindow.h \
    paintarea.h \
    algorithms/brasenham.h \
    algorithms/abstractalgorithm.h \
    algorithms/dda.h \
    algorithms/wu.h \
    point/point.h
